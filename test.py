# -*- coding: utf-8 -*-
from silf.backend.att_dubna.utils import GrzesiekPort
from silf.backend.commons.io.serialbase import PARITY_NONE

port = GrzesiekPort('/dev/ttyUSB0', 9600, 8, 1, PARITY_NONE, 1)

for ii in range(100):
    print(port.send_command("AT+RS\n\r", 1))