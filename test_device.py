# -*- coding: utf-8 -*-
import time
from datetime import timedelta
from silf.backend.att_dubna.driver import RealDriver
from silf.backend.att_dubna.utils import GrzesiekPort
from silf.backend.commons.io.serialbase import PARITY_NONE

driver = RealDriver(
    GrzesiekPort('/dev/ttyUSB0', 9600, 8, 1, PARITY_NONE, 2)
)

driver.start('lead', 10, timedelta(minutes=2))

while driver.is_measuring:
    time.sleep(1)
    print(driver.get_results())