
# -*- coding: utf-8 -*-
from silf.backend.commons.io.serialbase import SerialError
from silf.backend.commons.io.text_serial import TextSerial


class UnknownResponseErrror(Exception):
    pass


class GrzesiekPort(TextSerial):

    def ok_error_command(self, command):
        # print(command)
        result = self.send_command(command, 1)
        result = result[0].strip()
        if result == "OK":
            return
        if result == "ERROR":
            raise SerialError("Error for {}".format(command))
        raise UnknownResponseErrror("Unknown response '{}'".format(result))

    def command_with_response_or_error(self, command, lines_of_response=1):
        result = []
        with self.with_opened_port():
            self.write_str(command)
            first_line = self.readline()
            if first_line.strip() == "ERROR":
                raise SerialError("Error for {}".format(command))
            result.append(first_line)
            for ii in range(lines_of_response - 1):
                result.append(self.readline())
        return result
