# -*- coding: utf-8 -*-
import configparser
import typing

from datetime import timedelta
from silf.drivers.light_relay.relay import RelayDriver
from silf.drivers.light_relay.relay_mock_driver import MockRelayDriver

from silf.backend.att_dubna.driver import RealDriver, Materials, MockDriver
from silf.backend.att_dubna.utils import GrzesiekPort
from silf.backend.commons.api import ControlSuite, ComboBoxControl, \
    NumberControl, OutputFieldSuite, OutputField, ResultSuite, ChartField
from silf.backend.commons.api import TimeControlMinSec
from silf.backend.commons.api.stanza_content._misc import ResultsStanza, Result
from silf.backend.commons.device_manager._result_creator import \
    XYChartResultCreator, OverrideResultsCreator
from silf.backend.commons.experiment._experiment_manager import \
    IExperimentManager, ExperimentCallback
from silf.backend.commons.simple_server.simple_server import ExperimentMode, \
    MultiModeManager, ResultManager
from silf.backend.commons.api.translations.trans import _


class ParticleCountChart(ChartField):
    def __init__(self):

        super().__init__("chart", ["chart_material"])
        self.title = _("NumberImpulsesTitle")
        self.axis_x_label = _("AbsorbentWidth")
        self.axis_y_label = _("NumberImpulses")


class BackgroundPerSecondChart(ChartField):
    def __init__(self):
        super().__init__("chart", ["chart_background"])
        self.title = _("BackgrounPerSecond")
        self.axis_x_label = _("Time")
        self.axis_y_label = _("ImpulsesPerSecond")


class BaseMode(ExperimentMode):

    CONFIG_SECTION = 'att_device'

    def __init__(self, config_parser, experiment_callback):
        super().__init__(config_parser, experiment_callback)
        self.started = False
        self.driver = None
        self.light_driver = None

    def start(self):
        super(BaseMode, self).start()
        self.started = True

    def stop(self):
        super(BaseMode, self).stop()
        self.started = False

    def tear_down(self):
        self.power_down()

    def power_down(self):
        if self.driver:
            self.driver.stop()
            self.driver.power_down()
            self.driver = None
        self.light_driver.off()
        self.light_driver.close()

    def power_up(self):
        # TODO: Configure port from properties
        assert isinstance(self.config, configparser.ConfigParser)
        if self.config.getboolean('att_device', 'mock'):
            self.driver = MockDriver()
            self.light_driver = MockRelayDriver(3)
        else:
            self.driver = RealDriver(
                GrzesiekPort.create_from_config_parser(self.config, 'att_device')
            )
            self.light_driver = RelayDriver(
                self.config.get('relay', 'GPIOPort', fallback=3)
            )
        # self.light_driver.
        self.light_driver.on()


class BackgroundMode(BaseMode):

    def __init__(self, config_parser, experiment_callback):
        super().__init__(config_parser, experiment_callback)
        self.result_manager = ResultManager(
            callback=self.experiment_callback,
            creators=[
                OverrideResultsCreator('time_left'),
                XYChartResultCreator("chart_background", read_from=["current_time", "background_per_sec"]),
            ])

    def stop(self):
        super().stop()
        self.driver.stop()
        self.result_manager.clear()

    def start(self):
        super().start()
        self.driver.start_background(self.settings['acquisition_time'])
        self.result_manager.clear()

    def loop_iteration(self):
        if self.driver is None:
            return
        if self.started:
            if not self.driver.is_measuring:
                self.started = False
                self.experiment_callback.send_series_done()
            results = self.driver.get_results()

            elapsed = (self.settings['acquisition_time'] - results.time_left)
            if elapsed.total_seconds() == 0:
                return
            self.result_manager.push_results({
                'time_left': results.time_left.total_seconds(),
                'current_time': elapsed.total_seconds(),
                'background_per_sec': results.impulses / elapsed.total_seconds()
            })

    @classmethod
    def get_series_name(self, settings):
        return _("BackgroundSeries")

    @classmethod
    def get_output_fields(self):
        return OutputFieldSuite(
            chart_background=BackgroundPerSecondChart(),
            # background_per_sec=OutputField(
            #     "integer-indicator", ["background_per_sec"], label="Aktualna wartość pomiaru tła na sekundę"
            # ),
            time_left=OutputField(
                "interval-indicator", ["time_left"], label=_("BackgroundTimeLeft")
            ),
            # particle_count=OutputField(
            #     "integer-indicator", ["particle_count"], label="Liczba zliczeń"
            # )
        )

    @classmethod
    def get_input_fields(self) -> ControlSuite:
        return ControlSuite(
            TimeControlMinSec(
                "acquisition_time", label=_("BackgroundMeasurementTime"),
                min_time=timedelta(seconds=5), max_time=timedelta(minutes=10),
                default_value=timedelta(seconds=180)))

    @classmethod
    def get_description(cls) -> str:
        return _("BackgroundModeDesc")

    @classmethod
    def get_mode_label(cls):
        return _("BackgroundModeLabel")

    @classmethod
    def get_mode_name(cls) -> str:
        return "background"


class MaterialMode(BaseMode):

    def __init__(self, config_parser, experiment_callback):
        super().__init__(config_parser, experiment_callback)
        self.started = False
        self.current_width_idx = 0
        self.result_manager = ResultManager(
            callback=self.experiment_callback,
            creators=[
                OverrideResultsCreator('width'),
                OverrideResultsCreator('time_left'),
                XYChartResultCreator('chart_material', read_from=['width', 'impulses'])
            ])

    @classmethod
    def get_description(cls) -> str:
        return _("MaterialModeDesc")

    @classmethod
    def get_mode_label(cls):
        return _("MaterialModeLabel")

    @classmethod
    def get_mode_name(cls) -> str:
        return "material"

    @classmethod
    def get_output_fields(cls):
        return OutputFieldSuite(
            chart_material=ParticleCountChart(),
            time_left=OutputField(
                "interval-indicator", ["time_left"],
                label=_("MaterialTimeLeft")
            )
        )

    @classmethod
    def get_input_fields(cls):
        return ControlSuite(
            ComboBoxControl(
                "material_type", label=_("MaterialType"),
                choices=Materials.material_name_map()),
            TimeControlMinSec(
                "acquisition_time",
                label=_("MeasurementTime"),
                min_time=timedelta(seconds=5), max_time=timedelta(minutes=10),
                default_value=timedelta(seconds=180)),
            # TODO: Enable it back
            # NumberControl(
            #     "background_value", label="Liczba zliczeń impulsów tła na sekunde", type="number", style="gauge",
            #     defaultelse:_value=0, live=False, validators=None, required=True, description=None,
            #     min_value=0)
        )

    def __send_results(self):
        results = self.driver.get_results()
        self.result_manager.push_results({
            'width': self.material.widths[self.current_width_idx],
            'impulses': results.impulses,
            'time_left': results.time_left.total_seconds()
        })

    def __start_next_pont(self) -> bool:
        self.current_width_idx += 1
        if self.current_width_idx >= len(self.material.widths):
            return False
        self.driver.start(
            self.material.name,
            self.material.widths[self.current_width_idx],
            self.acq_time
        )
        return True

    def apply_settings(self, settings):
        super().apply_settings(settings)
        self.material = Materials.by_name(self.settings['material_type'])
        self.acq_time = self.settings['acquisition_time']

    def start(self):
        super(MaterialMode, self).start()
        self.current_width_idx = -1
        self.__start_next_pont()

    def stop(self):
        super(MaterialMode, self).stop()
        self.current_width_idx = 0
        self.driver.stop()
        self.result_manager.clear()

    def loop_iteration(self):
        if self.driver is None:
            return
        if self.started:
            if self.driver.is_measuring:
                self.results = self.driver.get_results()
                self.__send_results()
            else:
                if not self.__start_next_pont():
                    assert isinstance(self.experiment_callback, ExperimentCallback)
                    self.experiment_callback.send_series_done()
                    self.stop()

    @classmethod
    def get_series_name(self, settings):
        return "{}, {}: {}".format(
            _(Materials.by_name(settings['material_type'])[0]),
            _("time"),
            settings['acquisition_time'].total_seconds()
        )


class AttManager(MultiModeManager):

    @classmethod
    def get_mode_managers(cls, config_parser: configparser.ConfigParser) -> typing.List['ExperimentMode']:
        return [
            BackgroundMode,
            MaterialMode
        ]

    def post_power_up_diagnostics(self, *args, **kwargs):
        pass

    def pre_power_up_diagnostics(self, *args, **kwargs):
        pass

    EXPERIMENT_NAME = "Attenuaiton"

    MODE_MANAGERS = {
        'Material': MaterialMode,
        'Background': BackgroundMode
    }
