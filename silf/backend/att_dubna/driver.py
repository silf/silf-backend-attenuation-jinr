# -*- coding: utf-8 -*-
import abc

import enum
from collections import namedtuple, OrderedDict
from datetime import timedelta
import time

from silf.backend.att_dubna.utils import GrzesiekPort, UnknownResponseErrror
from silf.backend.commons.api.exceptions import SilfException
from silf.backend.commons.io import serial
from silf.backend.commons.io.serial import long_transaction
from silf.backend.commons.io.serialbase import SerialBase, SerialError
from silf.backend.commons.api.translations.trans import _

MaterialDesc = namedtuple('MaterialDesc', ['name', 'offset', 'widths'])

# MeasurementResults = namedtuple("MeasurementResults", ['impulses', 'time_left', 'is_measuring'])

MeasurementResults = namedtuple("MeasurementResults", ['impulses', 'time_left', 'is_measuring', 'on_position'])


class Materials(enum.Enum):

    LEAD = MaterialDesc(
        _("Lead"),
        1,
        [1, 5, 7, 10, 15, 20]
    )

    COOPER = MaterialDesc(
        _("Cooper"),
        7,
        [1, 5, 7, 10, 15, 20]
    )

    ALUMINIUM = MaterialDesc(
        _("Aluminium"),
        13,
        [1, 5, 10, 15, 20, 30]
    )

    @classmethod
    def by_name(cls, name) -> MaterialDesc:
        return cls.__members__[name.upper()].value

    @classmethod
    def material_name_map(cls):
        res = OrderedDict()
        for e in sorted(cls.__members__.keys()):
            res[e] = cls.__members__[e].value.name
        return res

    @classmethod
    def material_index_from_name_and_width(cls, name, width):
        mat = cls.by_name(name)
        # assert isinstance(mat.widths, list)
        index = mat.widths.index(width)
        return index + mat.offset


class IDriver(object, metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def start(self, material_name: str, width: int, time: timedelta) -> None:
        pass

    @abc.abstractmethod
    def stop(self):
        pass

    @abc.abstractmethod
    def get_results(self) -> MeasurementResults:
        pass

    @abc.abstractmethod
    def start_background(self, set_time: timedelta):
        pass

    @abc.abstractmethod
    def power_down(self):
        pass

    @property
    def is_measuring(self):
        res = self.get_results()
        return res.is_measuring


class MockDriver(IDriver):

    def __init__(self):
        super().__init__()
        self.start_time = None
        self.last_results = 0

    def stop(self):
        self.start_time = None

    def power_down(self):
        pass

    def is_measuring(self):
        return self.start is not None

    def start_background(self, set_time: timedelta):
        self.width = 100
        self.start_time = time.monotonic()
        self.last_results = 0
        self.acq_time = set_time


    def start(self, material_name: str, width: int, acq_time: timedelta) -> None:
        self.material_name = material_name
        self.width = width
        self.acq_time = acq_time
        self.start_time = time.monotonic()
        self.last_results = 0

    def get_results(self) -> MeasurementResults:
        if self.start_time is None:
            return MeasurementResults(
                self.last_results, timedelta(), False, True
            )

        elapsed = time.monotonic() - self.start_time
        if elapsed > self.acq_time.total_seconds():
            self.start_time = None
            self.last_results = elapsed * self.width
        return MeasurementResults(
            elapsed * self.width, timedelta(seconds=elapsed), True, True
        )


class RealDriver(IDriver):
    def __init__(self, port: GrzesiekPort):
        super().__init__()
        self.port = port

    @long_transaction(caught_exceptions=[SerialError, ])
    def stop(self):
        with self.port.with_opened_port():
            self.port.ok_error_command("AT+W={m}:{s},{idx}\r\n".format(
                m=0,
                s=30,
                idx=19
            ))
            self.__wait_while_on_position()
            if self.is_measuring:
                self.port.ok_error_command("AT+S=2\r\n")

    @long_transaction(caught_exceptions=[SerialError, UnknownResponseErrror])
    def start_background(self, set_time: timedelta):
        with self.port.with_opened_port():
            sec = int(set_time.total_seconds())
            self.port.ok_error_command("AT+W={m}:{s},{idx}\r\n".format(
                m=sec // 60,
                s=sec % 60,
                idx=19
            ))
            self.__wait_while_on_position()
            self.port.ok_error_command("AT+S=1\r\n")

    @long_transaction(caught_exceptions=[SerialError, UnknownResponseErrror])
    def start(self, material_name: str, width: int, set_time: timedelta) -> None:
        with self.port.with_opened_port():
            sec = int(set_time.total_seconds())
            idx = Materials.material_index_from_name_and_width(material_name, width)
            self.port.ok_error_command("AT+W={m}:{s},{idx}\r\n".format(
                m=sec // 60,
                s=sec % 60,
                idx=idx
            ))

            self.__wait_while_on_position()
            self.port.ok_error_command("AT+S=1\r\n")


    def __wait_while_on_position(self):
        time.sleep(0.1)
        start = time.monotonic()
        time.sleep(0.1)
        on_position = self._get_results_impl().on_position
        while (time.monotonic() - start) < 30 and not on_position:
            on_position = self._get_results_impl().on_position
        if not on_position:
            raise SilfException("Couldn't go to positon")


    def _get_results_impl(self):
        with self.port.with_opened_port():
            response = self.port.command_with_response_or_error("AT+RS\r\n")[0]
            # print("response is '{}'".format(response))
            if len(response.strip()) == 0:
                time.sleep(0.1)
                raise SerialError("Unknown response")

        parts = response.split(',')
        time_left_raw = parts[0].split(':')
        time_left = timedelta(
            minutes=int(time_left_raw[0]),
            seconds=int(time_left_raw[1]))

        absorber, counts, is_measuring, on_position = map(int, parts[1:])

        #TODO: CHeck if proper absorber is set

        on_position = bool(on_position)
        is_measuring = bool(is_measuring)

        res = MeasurementResults(
            counts, time_left, is_measuring, on_position
        )

        return res

    def power_down(self):
        self.stop()
        self.port.close_port()

    @long_transaction(caught_exceptions=[SerialError, ])
    def get_results(self) -> MeasurementResults:
        return self._get_results_impl()
