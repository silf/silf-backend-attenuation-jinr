from configparser import ConfigParser
import random
import time
from silf.backend.commons.api import ResultSuite
from silf.backend.commons.api.stanza_content._misc import SettingSuite, Setting
from silf.backend.commons.experiment._experiment_manager import ExperimentCallback
from silf.backend.att_dubna.manager import MaterialMode

__author__ = 'jb'

DONE = False

class SimpleCallback(ExperimentCallback):
  def send_series_done(self, message: str=None):
    print("Series done")
    global DONE
    DONE = True

  def send_results(self, results: ResultSuite):
    print(results.__getstate__())

  def send_experiment_done(self, message: str=None):
    print("Stop")

cp = ConfigParser()
cp.read("experiment.ini")

mode = MaterialMode(
  cp, SimpleCallback()
)

while True:

  DONE = False

  mode.power_up()

  mode.apply_settings(
   SettingSuite(
     material_type=Setting(value=random.choice(['LEAD', 'ALUMINIUM', 'COOPER'])),
     # material_type=Setting(value= 'COOPER'),
     acquisition_time=Setting(value=30),
   )
  )

  mode.start()

  while not DONE:
    mode.loop_iteration()
    time.sleep(.1)

  mode.power_down()