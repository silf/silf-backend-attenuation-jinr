��          �               l     m  	   |     �     �     �     �     �     �     �                    -     ?     P     ]     m     |     �     �  �  �     (  	   =  (   G  "   p     �     �     �  (   �                 &   !     H  ;   Q     �     �     �  5   �     �        AbsorbentWidth Aluminium BackgrounPerSecond BackgroundMeasurementTime BackgroundModeDesc BackgroundModeLabel BackgroundSeries BackgroundTimeLeft Cooper ImpulsesPerSecond Lead MaterialModeDesc MaterialModeLabel MaterialTimeLeft MaterialType MeasurementTime NumberImpulses NumberImpulsesTitle Time time Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-11-18 17:58+0100
PO-Revision-Date: 2017-11-18 16:36+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
 Absorbent width [mm] Aluminium Number of Background Impulses per Second Measurement time of background [s] Measures background radiation Background radiation Background Series Time left to end of background check [s] Cooper Impulses per second Lead Measures attenuation in given material Material Time left to the end of measurement for current width [sek] Material type Measurement Time Number of recorded impulses Number of Recorded Impulses for Given Absorbent Width Time [s] time 