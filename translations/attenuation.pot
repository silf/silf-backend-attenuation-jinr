# Translations template for PROJECT.
# Copyright (C) 2017 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2017.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2017-11-18 17:58+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: silf/backend/att_dubna/driver.py:26
msgid "Lead"
msgstr ""

#: silf/backend/att_dubna/driver.py:32
msgid "Cooper"
msgstr ""

#: silf/backend/att_dubna/driver.py:38
msgid "Aluminium"
msgstr ""

#: silf/backend/att_dubna/manager.py:28
msgid "NumberImpulsesTitle"
msgstr ""

#: silf/backend/att_dubna/manager.py:29
msgid "AbsorbentWidth"
msgstr ""

#: silf/backend/att_dubna/manager.py:30
msgid "NumberImpulses"
msgstr ""

#: silf/backend/att_dubna/manager.py:36
msgid "BackgrounPerSecond"
msgstr ""

#: silf/backend/att_dubna/manager.py:37
msgid "Time"
msgstr ""

#: silf/backend/att_dubna/manager.py:38
msgid "ImpulsesPerSecond"
msgstr ""

#: silf/backend/att_dubna/manager.py:128
msgid "BackgroundSeries"
msgstr ""

#: silf/backend/att_dubna/manager.py:138
msgid "BackgroundTimeLeft"
msgstr ""

#: silf/backend/att_dubna/manager.py:149
msgid "BackgroundMeasurementTime"
msgstr ""

#: silf/backend/att_dubna/manager.py:155
msgid "BackgroundModeDesc"
msgstr ""

#: silf/backend/att_dubna/manager.py:159
msgid "BackgroundModeLabel"
msgstr ""

#: silf/backend/att_dubna/manager.py:182
msgid "MaterialModeDesc"
msgstr ""

#: silf/backend/att_dubna/manager.py:186
msgid "MaterialModeLabel"
msgstr ""

#: silf/backend/att_dubna/manager.py:198
msgid "MaterialTimeLeft"
msgstr ""

#: silf/backend/att_dubna/manager.py:206
msgid "MaterialType"
msgstr ""

#: silf/backend/att_dubna/manager.py:210
msgid "MeasurementTime"
msgstr ""

#: silf/backend/att_dubna/manager.py:272
msgid "time"
msgstr ""

